import { Post } from "../models/post.model.js";

const postCtrl = {};

postCtrl.getPosts = async (req, res) => {
	try {
		const posts = await Post.find();
		return res.json(posts);
	} catch (err) {
		console.error("Error al obtener posts de la BD", err);
		res.status(500).json({"message": "Error al obtener las publicaciones de la base de datos"});
	}
}

postCtrl.getPost = async (req, res) => {
	try {
		const { _id } = req.params;
		const post = await Post.findById(_id);
		if (!post) {
			return res.json({"message": "No se encontró la publicación"});
		}
		return res.json(post);
	} catch (err) {
		console.error("Error al obtener post de la BD", err);
		res.status(500).json({"message": "Error al obtener la publicación de la base de datos"});
	}
}

postCtrl.postPost = async (req, res) => {
	try {
		const { titulo, descripcion, imagen, autor } = req.body.post;
		const newPost = new Post({
			"titulo": titulo,
			"descripcion": descripcion,
			"imagen": imagen,
			"autor": autor
		});
		const saved = await newPost.save();
		return res.json(saved);
	} catch (err) {
		console.error("Error al guardar post en la BD", err);
		res.status(500).json({"message": "Error al guardar la publicación en la base de datos"});
	}
}

postCtrl.deletePost = async (req, res) => {
	try {
		const { _id } = req.params;
		const deleted = await Post.findByIdAndDelete(_id);
		if (!deleted) {
			return res.json({"message": "No se encontró la publicación"});
		}
		return res.json(deleted);
	} catch (err) {
		console.error("Error al eliminar post de la BD", err);
		res.status(500).json({"message": "Error al eliminar la publicación de la base de datos"});
	}
}

postCtrl.updatePost = async (req, res) => {
	try {
		const { _id } = req.params;
		const { titulo, descripcion, imagen } = req.body;
		const old = await Post.findById(_id);
		if (!old) {
			return res.json({"message": "No se encontró la publicación"});
		}
		if (titulo) old.titulo = titulo;
		if (descripcion) old.descripcion = descripcion;
		if (imagen) old.imagen = imagen;
		const updated = await old.save();
		return res.json(updated);
	} catch (error) {
		console.error("Error al actualizar la publicación: ", error);
		res.json({"message": "Error al actualizar la publicación"})
	}
}

postCtrl.addComment = async (req, res) => {
	try {
		const { _id } = req.params;
		const { comment } = req.body;
		const post = await Post.findById(_id);
		if (!post) {
			return res.json({"message": "No se encontró la publicación"});
		}
		post.comentarios.push(comment);
		const updated = await post.save();
		return res.json({"message": "Comentario guardado", "post": updated});
	} catch (err) {
		console.error("Error al guardar comentario en la BD", err);
		res.status(500).send("Error al guardar el comentario de la publicación");
	}
}

export { postCtrl };
