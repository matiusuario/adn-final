import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import { User } from "../models/user.model.js";
import { SECRET } from "../config.js";

const userCtrl = {};

userCtrl.signUpUser = async (req, res) => {
	try {
		const { usuario, password, email } = req.body;
		const hashedPassword = await bcrypt.hash(password, 10);
		const newUser = new User({
			"usuario": usuario,
			"password": hashedPassword,
			"email": email
		});
		const saved = await newUser.save();
		return res.status(201).json(saved);
	} catch (err) {
		console.error("Error al guardar user en la BD", err);
		res.status(500).send("Error al registrar el usuario");
	}
}

userCtrl.loginUser = async (req, res) => {
	try {
		const { usuario, password } = req.body;
		const user = await User.findOne({ usuario });
		if (!user) {
			return res.status(404).json({"message": "usuario o contraseña incorrectos"});
		}
		const isCorrect = await bcrypt.compare(password, user.password);
		if (!isCorrect) {
			return res.status(404).json({"message": "usuario o contraseña incorrectos"});
		}
		const token = jwt.sign({_id: user._id}, SECRET, {expiresIn: "1h"});
		return res.status(201).json({token, "user": user._id});
	} catch (err) {
		console.error("Error al iniciar sesión", err);
		res.status(500).send("Error al iniciar sesión");
	}
}

export { userCtrl };
