import jwt from "jsonwebtoken";
import { User } from "../models/user.model.js";
import { SECRET } from "../config.js";

const authenticate = async (req, res, next) => {

	try {
		const token = req.headers["authorization"];
		const { user } = req.body;
		if (!token) {
			return res.sendStatus(401);
		}
		const fetchedUser = await User.findById(user);

		if (!fetchedUser) {
			return res.sendStatus(401);
		}
		jwt.verify(token, SECRET, (err, user) => {
			if (err) {
				return res.sendStatus(401);
			}
			req.user = fetchedUser;
			next();
		});
	} catch (error) {
		return res.sendStatus(401);
	}
}

export { authenticate };