import { model, Schema } from "mongoose";

const PostSchema = new Schema({
	titulo: {
		type: String,
		required: true,
		minlength: 2
	},
	descripcion: {
		type: String,
		required: true,
		minlength: 2
	},
	imagen: {
		type: String,
	},
	comentarios: [{
		contenido: {
			type: String,
			required: true
		},
		autor: {
			type: Schema.Types.ObjectId,
			ref: "User",
			required: true,
		}
	}],
	autor: {
		type: Schema.Types.ObjectId,
		ref: "User",
		required: true,
	}
});

const Post = model("post", PostSchema);

export { Post };
