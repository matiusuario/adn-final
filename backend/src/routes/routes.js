import { Router } from "express";
import { postCtrl } from "../controllers/post.controller.js";
import { userCtrl } from "../controllers/user.controller.js";
import { authenticate } from "../middlewares/authenticate.js";

const router = Router();

router.get("/", (req, res) => {
    res.send("Hola, mundo!");
});

router.post("/posts", authenticate, postCtrl.postPost);

router.get("/posts", postCtrl.getPosts);

router.get("/posts/:_id", postCtrl.getPost);

router.delete("/posts/:_id", authenticate, postCtrl.deletePost);

router.put("/posts/:_id", authenticate, postCtrl.updatePost);

router.put("/posts/:_id/comments", authenticate, postCtrl.addComment);

router.post("/users/signup", userCtrl.signUpUser);

router.post("/users/login", userCtrl.loginUser);

export { router };
