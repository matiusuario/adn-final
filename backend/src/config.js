import 'dotenv/config';

export const PORT = process.env.PORT || 4000;
export const DB_NAME = process.env.DB_NAME;
export const MONGODB_URI = process.env.MONGODB_URI;
export const SECRET = process.env.SECRET;
