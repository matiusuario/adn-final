import { connect } from "mongoose";
import { DB_NAME, MONGODB_URI } from "./config.js";

export const connectDb = async () => {
	try {
		const db = await connect(MONGODB_URI, {
			dbName: DB_NAME
		});
		console.log(`Conectado a la base de datos ${db.connection.name}`);
	} catch (error) {
		console.error("error conectando a la base de datos", error);
	}
}
