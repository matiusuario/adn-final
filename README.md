# Proyecto final
## Sistema CRUD de publicaciones

Utiliza stack MERN  
MongoDB, Express, React, Node.js

## Ejecutar

### Requisitos del sistema:

- Node.js

Si se quiere una base de datos local:
- MongoDB

### Luego de clonar:

- En el directorio backend dar valores a las variables definidas en .env.example y renombrar archivo a .env  
(Opcionalmente también en directorio frontend, por defecto http://localhost y 4000.  
VITE_SERVER_PORT de frontend debe coincidir con PORT de backend)

- En una terminal en el directorio backend ejecutar:  
npm install  
node .

- En una terminal en el directorio frontend ejecutar:  
npm install  
npm run dev

- Abrir el navegador en el servidor y puerto especificado por Vite, típicamente [http://localhost:5173/](http://localhost:5173/)
