import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AuthProvider from './providers/AuthProvider';
import "./styles/style.css";
import Header from './components/Header';
import Footer from './components/Footer';
import Home from './pages/Home';
import Publish from './pages/Publish';
import Post from './pages/Post';
import Login from './pages/Login';
import SignUp from './pages/SignUp';

const App = () => {
	return (
		<AuthProvider>
		<BrowserRouter>
			<Header />
			<Routes>
				<Route path='/' element={<Home />}/>
				<Route path='/publicar' element={<Publish />}/>
				<Route path='/publicacion/:_id' element={<Post />}/>
				<Route path='/ingresar' element={<Login />}/>
				<Route path='/registrarse' element={<SignUp />}/>
			</Routes>
			<Footer />
		</BrowserRouter>
		</AuthProvider>
	)
}

export default App;
