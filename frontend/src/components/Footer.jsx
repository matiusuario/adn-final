import { Link } from "react-router-dom";
import logo from "../assets/logo-publitodo-1.webp";

const Footer = () => {

	return (
		<footer className="footer d-flex flex-wrap justify-content-between align-items-center px-3 py-5 mt-auto border-top bg-dark" data-bs-theme="dark">
			<div className="col-md-4 d-flex align-items-center">
				<Link to="/" className="mb-3 me-2 mb-md-0 text-body-secondary text-decoration-none lh-1">
					<img src={logo} className="logo" alt="logotipo del sitio"/>
				</Link>
				<span className="mb-3 mb-md-0 text-body-secondary">&copy; 2024 Publitodo</span>
			</div>
		</footer>
	)
}

export default Footer;
