import { useState, useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import { domain } from "../config/utils";
import { AuthContext } from "../providers/AuthProvider";

const CommentForm = ({ post }) => {

	const [comm, setComm] = useState("");
	const navigate = useNavigate();
	const { auth } = useContext(AuthContext);

	const handleAddComment = async (e) => {
		e.preventDefault();
		const comment = {};
		const user = auth.user;
		comment.contenido = comm;
		comment.autor = user;
		const req = await fetch(`${domain}/posts/${post._id}/comments`, {
			method: "PUT",
			body: JSON.stringify({comment, user}),
			headers: {
				"Content-Type": "application/json",
				"Authorization": auth.token
			}
		});
		if (req.status >= 400 ) {
			alert("Error al guardar comentario");
			return;
		}
		setComm("");
		post.comentarios.push(comment);
		alert("Se guardó el comentario");
		navigate(`/publicacion/${post._id}`);
	}

	return (
		<div>
			<h3 className="mt-4">Agregar comentario</h3>
			{auth ? 
				<form onSubmit={handleAddComment}>
					<div className="mb-3">
						<label htmlFor="commentText" className="form-label">Nuevo comentario</label>
						<textarea required type="text" className="form-control" id="commentText" 
							name="commentText" value={comm} onChange={(e) => setComm(e.target.value)}>
						</textarea>
					</div>
					<button type="submit" className="btn btn-primary">Agregar comentario</button>
				</form> 
				: <span className="fs-5">
					<Link to="/ingresar" className="">Ingresa</Link> para comentar
				</span>
			}
		</div>
	)
}

export default CommentForm;
