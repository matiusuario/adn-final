import { useNavigate } from 'react-router-dom';
import { useContext } from 'react';
import { AuthContext } from "../providers/AuthProvider.jsx";
import icEliminar from '../assets/trash-can-regular.svg';
import { domain } from '../config/utils.js';

const PostItem = ({ post }) => {

	const navigate = useNavigate();
	const { auth } = useContext(AuthContext);

	const handleDeletePost = async () => {
		let confirmed = confirm("¿Seguro que desea eliminar la publicación?");
		if (!confirmed) {
			return;
		}
		const user = auth.user;
		const req = await fetch(`${domain}/posts/${post._id}`, {
			method: "DELETE",
			body: JSON.stringify({user}),
			headers: {
				"Content-Type": "application/json",
				"Authorization": auth.token
			}
		});
		if (req.status >= 400 ) {
			alert("Error al eliminar publicación");
			navigate("/");
			return;
		}
		alert("Publicación eliminada con éxito.");
		navigate("/");
	}

	return (
		<div className="col">
			<div className="card h-100">
				{post.imagen && <img src={post.imagen} className="card-img-top" alt="imagen de la publicación"/>
				}
				<div className="card-body">
					<h5 className="card-title">{post.titulo}</h5>
					<p className="card-text">{post.descripcion}</p>
				</div>
				<div className="card-footer d-flex">
					<button type="button" className="btn btn-secondary" onClick={() => navigate(`/publicacion/${post._id}`)}>Más información</button>
					{ auth && auth.autor === post.usuario && 
						<button type="button" className="btn btn-outline-secondary mx-3" onClick={handleDeletePost}>
							<img src={icEliminar} alt="icono de eliminar" className="ic"/>
						</button>
					}
				</div>
			</div>
		</div>
	)
}

export default PostItem;
