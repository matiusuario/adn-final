import { Link } from "react-router-dom";
import { useContext } from "react";
import { AuthContext } from "../providers/AuthProvider.jsx";
import logo from "../assets/logo-publitodo-1.webp";

const Header = () => {
	
	const { auth, logout } = useContext(AuthContext);

	const logoutDialog = () => {
		let confirmed = confirm("¿Seguro que desea cerrar sesión?");
		if (confirmed) {
			logout();
		}
	}

	return (
		<header>
			<nav className="navbar navbar-expand-lg bg-dark" data-bs-theme="dark">
				<div className="container-fluid">
					<Link to="/" className="navbar-brand">
						<img src={logo} className="logo" alt="logotipo del sitio"/>
					</Link>
					<button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggler" 
						aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
						<span className="navbar-toggler-icon"></span>
					</button>
					<div className="collapse navbar-collapse" id="navbarToggler">
						<ul className="navbar-nav me-auto">
							<li className="nav-item">
								<Link to="/" className="nav-link">Publicaciones</Link>
							</li>
							<li className="nav-item">
								<Link to="/publicar" className="nav-link">Publicar</Link>
							</li>
						</ul>
						<div className="d-flex navbar-nav">
							{auth ? <button type="button" className="btn nav-link" onClick={logoutDialog}>
								Cerrar sesión
							</button> : <Link to="/ingresar" className="nav-link">Ingresar</Link>
							}
						</div>
					</div>
				</div>
			</nav>
		</header>
	)
}

export default Header;
