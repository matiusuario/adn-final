const CommentItem = ({ comment }) => {

	return (
		<div className="row">
			<div className="comment">
				<p>{comment.contenido}</p>
			</div>
		</div>
	)
}

export default CommentItem;