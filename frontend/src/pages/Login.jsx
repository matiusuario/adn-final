import { Link, useNavigate } from "react-router-dom";
import { domain } from "../config/utils";
import { useContext } from "react";
import { AuthContext } from "../providers/AuthProvider.jsx";


const Login = () => {

	const navigate = useNavigate();
	const { login } = useContext(AuthContext);

	const handleSubmit = async (e) => {
		e.preventDefault();
		const formData = new FormData(e.target);
		const user = {};
		user.usuario = formData.get("usuario");
		user.password = formData.get("password");
		const req = await fetch(`${domain}/users/login`, {
			method: "POST",
			body: JSON.stringify(user),
			headers: {
				"Content-Type": "application/json",
			}
		});
		if (req.status === 404 ) {
			alert("Usuario o contraseña incorrecto");
			return;
		}
		if (req.status >= 400 ) {
			alert("Error al iniciar sesión");
			return;
		}
		const res = await req.json();
		login(res);
		navigate("/");
	}

	return (
		<div className="body">
			<h1>Inicia sesión</h1>
			<p>¿No tienes una cuenta? <Link to="/registrarse">Registrate</Link></p>
			<form className="m-3" onSubmit={handleSubmit}>
			<div className="mb-3">
					<label htmlFor="usuario" className="form-label">Usuario</label>
					<input required type="text" className="form-control" id="usuario" name="usuario"/>
				</div>
				<div className="mb-3">
					<label htmlFor="password" className="form-label">Contraseña</label>
					<input required type="password" className="form-control" id="password" name="password"></input>
				</div>
				<button type="submit" className="btn btn-primary">Ingresar</button>
			</form>
		</div>
	)
  }
  
  export default Login;
