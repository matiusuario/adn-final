import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { domain } from "../config/utils.js";
import PostItem from "../components/PostItem";

const Home = () => {

	const navigate = useNavigate();
	const [posts, setPosts] = useState([]);

	useEffect(() => {
		const fetchPosts = async () => {
			const response = await fetch(`${domain}/posts`);
			const fetchedPosts = await response.json();
			if (fetchedPosts.length) {
				setPosts(fetchedPosts);
			}
		}
		fetchPosts();
	}, []);

	return (
		<div className="body">
			<h1>Publitodo</h1>
			<div className="row row-cols-1 row-cols-md-3 g-4">
				{posts.map((post) => <PostItem post={post} key={post._id}/>)}
			</div>
			{posts.length === 0 && <div className="py-5">
				<p className="fs-4">Aún no hay publicaciones. Crea una!</p>
				<button onClick={() => navigate("/publicar")} className="btn btn-primary">
					Publicar
				</button>
			</div>
			}
		</div>
	)
}

export default Home;
