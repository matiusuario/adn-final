import { Link, useNavigate } from "react-router-dom";
import { domain } from "../config/utils";

const SignUp = () => {

	const navigate = useNavigate();

	const handleSubmit = async (e) => {
		e.preventDefault();
		const formData = new FormData(e.target);
		const user = {};
		user.usuario = formData.get("usuario");
		user.password = formData.get("password");
		user.email = formData.get("email");

		const req = await fetch(`${domain}/users/signup`, {
			method: "POST",
			body: JSON.stringify(user),
			headers: {
				"Content-Type": "application/json",
			}
		});
		if (req.status >= 400 ) {
			alert("Error al registrar usuario");
			navigate("/signup");
			return;
		}
		alert("Registrado con éxito. Redirigiendo...");
		setTimeout(() => {
			navigate("/ingresar");
		}, 1000);
	}

	return (
		<div className="body">
			<h1>Registrate</h1>
			<p>¿Ya estás registrado? <Link to="/ingresar">Inicia sesión</Link></p>
			<form className="signUpForm" onSubmit={handleSubmit}>
				<div className="mb-3">
					<label htmlFor="email" className="form-label">Email</label>
					<input required type="email" className="form-control" id="email" name="email"/>
				</div>
				<div className="mb-3">
					<label htmlFor="usuario" className="form-label">Usuario</label>
					<input required type="text" aria-describedby="usuarioHelp" 
						className="form-control"id="usuario" name="usuario"/>
					<div id="usuarioHelp" className="form-text">Debe poseer al menos 5 caracteres.</div>
				</div>
				<div className="mb-3">
					<label htmlFor="password" className="form-label">Contraseña</label>
					<input required type="password" aria-describedby="passwordHelp" 
						className="form-control" id="password" name="password"/>
					<div id="passwordHelp" className="form-text">Debe poseer al menos 5 caracteres.</div>
				</div>
				<button type="submit" className="btn btn-primary">Registrarse</button>
			</form>
		</div>
	)
  }
  
  export default SignUp;
