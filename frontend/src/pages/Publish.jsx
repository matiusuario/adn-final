import { Link, useNavigate } from "react-router-dom";
import { domain } from "../config/utils";
import { useContext } from "react";
import { AuthContext } from "../providers/AuthProvider";

const Publish = () => {

	const navigate = useNavigate();
	const { auth } = useContext(AuthContext);

	const handleSubmit = async (e) => {
		e.preventDefault();
		const formData = new FormData(e.target);
		const user = auth.user;
		const post = {};
		post.titulo = formData.get("title");
		post.descripcion = formData.get("description");
		post.imagen = formData.get("imageUrl");
		post.autor = user;
		const req = await fetch(`${domain}/posts`, {
			method: "POST",
			body: JSON.stringify({post, user}),
			headers: {
				"Content-Type": "application/json",
				"Authorization": auth.token
			}
		});
		if (req.status >= 400 ) {
			alert("Error al crear la publicación");
			return;
		}
		alert("Publicación creada con éxito");
		setTimeout(() => navigate("/"), 1000);
	}

	return (
		<div className="body">
			<h1>Crear una nueva publicación</h1>
			<form onSubmit={handleSubmit}>
				<div className="mb-3">
					<label htmlFor="title" className="form-label">Título</label>
					<input required type="text" className="form-control" id="title" name="title"/>
				</div>
				<div className="mb-3">
					<label htmlFor="imageUrl" className="form-label">URL de la imagen</label>
					<input type="text" className="form-control" id="imageUrl" name="imageUrl" placeholder="https://ejemplo.com/una-foto.png" aria-describedby="imageUrlHelp"/>
					<div id="imageUrlHelp" className="form-text">Ingresa el enlace a la imagen.</div>
				</div>
				<div className="mb-3">
					<label htmlFor="description" className="form-label">Cuerpo de la publicación</label>
					<textarea required type="text" className="form-control" id="description" name="description"></textarea>
				</div>
				{auth ? 
					<button type="submit" className="btn btn-primary">Publicar</button>
					: <span className="fs-5">
						<Link to="/ingresar" className="">Ingresa</Link> para publicar
					</span>
				}
			</form>
		</div>
	)
}

export default Publish;
