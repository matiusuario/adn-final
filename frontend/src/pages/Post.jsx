import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { domain } from "../config/utils";
import CommentItem from "../components/CommentItem"
import CommentForm from "../components/CommentForm";

const Post = () => {

	const navigate = useNavigate();
	const { _id } = useParams();
	const [post, setPost] = useState(null);

	useEffect(() => {
		const fetchPost = async () => {
			const response = await fetch(`${domain}/posts/${_id}`);
			const fetchedPost = await response.json();
			if (fetchedPost) {
				setPost(fetchedPost);
			}
		}
		fetchPost();
	}, []);

	

	return (
		<>
		{post && <div className="body">
			<h1 className="mb-5">{post.titulo}</h1>
			{post.imagen && <img src={post.imagen} className="img-fluid p-2 mb-5" alt="imagen de la publicación"/>
			}
			<p className="fs-5 mb-5">{post.descripcion}</p>
			<div className="mb-4">
				<h3 className="mb-4">Comentarios</h3>
				{post.comentarios.length === 0 && <p>No hay comentarios</p>}
				{post.comentarios.length !== 0 && 
					post.comentarios.map((c) => <CommentItem comment={c} key={c._id}/>)
				}
			</div>
			<CommentForm post={post}/>
			<button type="button" className="btn btn-secondary my-5" onClick={() => navigate("/")}>Atrás</button>
		</div>
		}
		</>
	)
}

export default Post;
