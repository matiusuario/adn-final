export const SERVER = import.meta.env.VITE_SERVER || "http://localhost";
export const SERVER_PORT = import.meta.env.VITE_SERVER_PORT || 4000;
export const domain = `${SERVER}:${SERVER_PORT}`;
